'''
Created on Feb 14, 2019

Part two: https://medium.com/@andrewklatzke/building-a-python-webserver-from-the-ground-up-part-two-c8ca336abe62

@author: Caleb Kofahl
'''

import time
from http.server import HTTPServer
from server import Server

HOST = 'localhost'
PORT = 8000

def main():
    httpd = HTTPServer((HOST, PORT), Server)
    print(time.asctime(), f'Server UP - {HOST}:{PORT}')
    try:
        httpd.serve_forever()
    except KeyboardInterrupt:
        pass
    httpd.server_close()
    print(time.asctime(), f'Server DOWN - {HOST}:{PORT}')

if __name__ == '__main__':
    main()